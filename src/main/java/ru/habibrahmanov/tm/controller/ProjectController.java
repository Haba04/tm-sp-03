package ru.habibrahmanov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.enumeration.Status;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

@Controller
public class ProjectController extends AbstractController {

    @RequestMapping(value = "/projectCreate", method = RequestMethod.POST)
    public String createProjectPost(
            @ModelAttribute("name") final String name,
            @ModelAttribute("description") final String description,
            @ModelAttribute("dateBegin") final String dateBegin,
            @ModelAttribute("dateEnd") final String dateEnd
    ) throws ParseException {
        projectService.insert(name, description, dateBegin, dateEnd);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/projectCreate", method = RequestMethod.GET)
    public String createProjectGet(@ModelAttribute("project") Project project) {
        return "projectCreate";
    }

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public String showAll(@NotNull final Model model) {
        List<Project> projectList = projectService.findAll();
        model.addAttribute("projectList", projectList);
        return "project";
    }

    @RequestMapping(value = "/projectView/{id}", method = RequestMethod.GET)
    public String showOne(final Model model, @PathVariable final String id) throws Exception {
        Project project = projectService.findOne(id);
        model.addAttribute("project", project);
        return "projectView";
    }

    @RequestMapping(value = "/projectRemove/{projectId}", method = RequestMethod.GET)
    public String removeOne(@PathVariable final String projectId) {
        projectService.removeOne(projectId);
        return "redirect:/projects";
    }

    @GetMapping(value = "/projectEdit/{id}")
    public String projectEditGet(final Model model, @PathVariable final String id) {
        model.addAttribute("project", projectService.findOne(id));
        model.addAttribute("status", Arrays.asList(Status.values()));
        return "projectEdit";
    }

    @PostMapping(value = "/projectEdit")
    public String projectEditPost(
            @ModelAttribute("id") final String id,
            @ModelAttribute("name") final String name,
            @ModelAttribute("description") final String description,
            @ModelAttribute("status") final Status status,
            @ModelAttribute("dateBegin") final String dateBegin,
            @ModelAttribute("dateEnd") final String dateEnd
    ) throws Exception {
        projectService.update(id, name, description, status, dateBegin, dateEnd);
        return "redirect:/projects";
    }
}