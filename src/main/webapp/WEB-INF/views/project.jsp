<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11.02.2020
  Time: 13:31
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="navbar.jsp" %>
    <title>Project List</title>
</head>
<body>
<p></p>
<p></p>

<table border="0" style="width: 80%; border-collapse: collapse; border-style: none; margin-left: auto; margin-right: auto;">
    <tbody>
    <tr>
        <%--        <td style="width: 50%;"><a href="/">HOME</a></td> <br>--%>
        <td style="width: 50%;"><a href="/projectCreate">CREATE PROJECT</a></td> <br>
    </tr>
    </tbody>
</table>
<table border="2" style="height: 26px; width: 80%; border-collapse: collapse; border-style: solid; margin-left: auto; margin-right: auto;"><caption>
    <tbody>
    <tr style="height: 18px;">
<%--        <td style="width: 4%; height: 18px;">No</td>--%>
        <td style="width: 27%; height: 18px;">ID</td>
        <td style="width: 16%; height: 18px;">NAME</td>
        <td style="width: 25%; height: 18px;">DESCRIPTION</td>
        <td style="width: 8%; height: 18px;">DATE BEGIN</td>
        <td style="width: 8%; height: 18px;">DATE END</td>
        <td style="width: 8%; height: 18px;">STATUS</td>
    </tr>
    <c:forEach var="project" items="${projectList}">
        <tr style="height: 18px;">
            <td style="width: 27%; height: 18px;">${project.id}</td>
            <td style="width: 16%; height: 18px;">${project.name}</td>
            <td style="width: 25%; height: 18px;">${project.description}</td>
            <td style="width: 25%; height: 18px;">${project.dateBegin}</td>
            <td style="width: 25%; height: 18px;">${project.dateEnd}</td>
            <td style="width: 25%; height: 18px;">${project.status}</td>
<%--            <td style="width: 8%; height: 18px;"><a href="/projectview">view</a></td>--%>
            <td style="width: 8%; height: 18px;"><a href="/projectView/${project.id}">view</a></td>
            <td style="width: 8%; height: 18px;"><a href="/projectEdit/${project.id}">edit</a></td>
            <td style="width: 8%; height: 18px;"><a href="/projectRemove/${project.id}">remove</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>



</body>
</html>