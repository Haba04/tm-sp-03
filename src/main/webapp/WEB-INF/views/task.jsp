<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11.02.2020
  Time: 13:31
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="navbar.jsp" %>
    <title>Project List</title>
</head>
<body>
<p></p>
<p></p>

<table border="0" style="width: 80%; border-collapse: collapse; border-style: none; margin-left: auto; margin-right: auto;">
    <tbody>
    <tr>
        <td style="width: 50%;"><a href="${pageContext.request.contextPath}/taskCreate/${projectId}">CREATE TASK</a></td> <br>
    </tr>
    </tbody>
</table>
<table border="2" style="height: 26px; width: 80%; border-collapse: collapse; border-style: solid; margin-left: auto; margin-right: auto;"><caption>
    <tbody>
    <tr style="height: 18px;">
        <td style="width: 27%; height: 18px;">PROJECT ID</td>
        <td style="width: 27%; height: 18px;">TASK ID</td>
        <td style="width: 16%; height: 18px;">NAME</td>
        <td style="width: 25%; height: 18px;">DESCRIPTION</td>
        <td style="width: 8%; height: 18px;">DATE BEGIN</td>
        <td style="width: 8%; height: 18px;">DATE END</td>
        <td style="width: 8%; height: 18px;">STATUS</td>
    </tr>
    <c:forEach var="task" items="${taskList}">
        <tr style="height: 18px;">
            <td style="width: 27%; height: 18px;">${task.project.id}</td>
            <td style="width: 27%; height: 18px;">${task.id}</td>
            <td style="width: 16%; height: 18px;">${task.name}</td>
            <td style="width: 25%; height: 18px;">${task.description}</td>
            <td style="width: 25%; height: 18px;">${task.dateBegin}</td>
            <td style="width: 25%; height: 18px;">${task.dateEnd}</td>
            <td style="width: 25%; height: 18px;">${task.status}</td>
            <td style="width: 8%; height: 18px;"><a href="${pageContext.request.contextPath}/taskEdit/${task.id}">edit</a></td>
            <td style="width: 8%; height: 18px;"><a href="${pageContext.request.contextPath}/taskRemove/${task.id}">remove</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>